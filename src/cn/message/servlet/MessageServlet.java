package cn.message.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.message.entity.Message;
import cn.message.service.MessageService;
import cn.message.service.impl.MessageServiceImpl;

/**
 * 短信息事件
 * @author ZENKIE
 *
 */
public class MessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//设置编码
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		//行为
		String action = request.getParameter("action");
		//判断行为
		if("main".equals(action)) {
			main(request,response);
		} else if("read".equals(action)) {
			read(request,response);
		} else if("delete".equals(action)) {
			delete(request,response);
		} else if("send".equals(action)) {
			send(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
	
	/**
	 * 发送短信息
	 * @param request 请求
	 * @param response 响应
	 * @throws ServletException
	 * @throws IOException
	 */
	private void send(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//获取发件人
		String name = (String)request.getSession().getAttribute("name");
		//未登录
		if(name == null || name.isEmpty()) {
			response.sendRedirect("index.jsp");
			return;
		}
		//获取收件人
		String sendto = request.getParameter("sendto");
		//获取标题
		String title = request.getParameter("title");
		//获取内容
		String content = request.getParameter("content");
		//数据不完整缺失
		if(sendto == null || sendto.isEmpty() || title == null || title.isEmpty() ||
				content == null || content.isEmpty()) {
			response.sendRedirect("main.jsp");
			return;
		}
		//提交
		Message msg = new Message();
		msg.setUsername(name);
		msg.setSendto(sendto);
		msg.setTitle(title);
		msg.setMsgcontent(content);
		//打印机
		PrintWriter out = response.getWriter();
		if(new MessageServiceImpl().add(msg) > 0) {
			out.print("<script>alert('发送成功！');location.href='main.jsp';</script>");
		} else {
			out.print("<script>alert('发送失败！');location.href='main.jsp';</script>");
		}
	}
	
	/**
	 * 删除
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException 
	 */
	private void delete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		//用户没有登录
		if(request.getSession().getAttribute("name") == null) {
			response.sendRedirect("index.jsp");
			return;
		}
		//获取编号
		String value = request.getParameter("msgid");
		//用户没有正确地进入网页
		if(value == null || value.isEmpty()) {
			//返回到消息列表
			response.sendRedirect("main.jsp");
			return;
		}
		int msgid = Integer.parseInt(value);
		//打印机
		PrintWriter out = response.getWriter();
		//删除
		if(new MessageServiceImpl().delete(msgid) > 0) {
			out.print("<script>alert('消息已删除！');location.href='main.jsp';</script>");
		} else {
			out.print("<script>alert('删除失败！');location.href='main.jsp';</script>");
		}
	}
	
	/**
	 * 主页，获取消息列表
	 * @param request 请求
	 * @param response 响应
	 * @throws ServletException
	 * @throws IOException
	 */
	private void main(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//短信息主页
		//获取收信人
		String sendto = (String)request.getSession().getAttribute("name");
		//没有正确的进入Servlet
		if(sendto == null || sendto.isEmpty()) {
			//跳转到主页
			response.sendRedirect("main.jsp");
			return;
		}
		request.setAttribute("msgs", new MessageServiceImpl().getMessagesBySendto(sendto));
		//转发到主页
		request.getRequestDispatcher("main.jsp").forward(request, response);
	}
	
	/**
	 * 读取信息
	 * @param request 请求
	 * @param response 响应
	 * @throws ServletException
	 * @throws IOException
	 */
	private void read(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		MessageService s = new MessageServiceImpl();
		//读取消息
		String value = request.getParameter("msgid");
		//非正常访问
		if(value == null || value.isEmpty()) {
			response.sendRedirect("index.jsp");
			return;
		}
		int msgid = Integer.parseInt(value);
		Message msg = s.getMessageByMsgid(msgid);
		//更新为已读
		if(msg.getState() == 0) {
			msg.setState(1);
			s.update(msg);
		}
		//保存到请求
		request.setAttribute("msg", msg);
		//转发
		request.getRequestDispatcher("read.jsp").forward(request, response);
	}

}
