package cn.message.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.message.entity.Message;
import cn.message.entity.UserInfo;
import cn.message.service.impl.MessageServiceImpl;
import cn.message.service.impl.UserServiceImpl;

/**
 * 用户事件
 * @author ZENKIE
 *
 */
public class UserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//设置编码
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		//操作事件
		String action = request.getParameter("action");
		//判断事件
		if("login".equals(action)) {
			login(request,response);
		} else if("regist".equals(action)) {
			regist(request,response);
		} else if("logout".equals(action)) {
			logout(request,response);
		} else if("send".equals(action)) {
			send(request, response);
		}
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	/**
	 * 准备发送短信息
	 * @param request 请求
	 * @param response 响应
	 * @throws IOException 
	 * @throws ServletException 
	 */
	private void send(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		//获取当前用户
		String name = (String)request.getSession().getAttribute("name");
		//没有登录直接访问
		if(name == null || name.isEmpty()) {
			//直接重定向到主页
			response.sendRedirect("index.jsp");
			return;
		}
		//保存其他用户
		request.setAttribute("others", new UserServiceImpl().getOthers(name));
		//获取要回复的消息编号
		String value = request.getParameter("msgid");
		if(value != null && !value.isEmpty()) {
			//转型
			int msgid = Integer.parseInt(value);
			//保存短信息
			request.setAttribute("msg", new MessageServiceImpl().getMessageByMsgid(msgid));
		}
		//转发
		request.getRequestDispatcher("send.jsp").forward(request, response);
	}
	
	/**
	 * 注销
	 * @throws IOException 
	 */
	private void logout(HttpServletRequest request, HttpServletResponse response) throws IOException {
		//会话失效
		request.getSession().invalidate();
		//重定向
		response.sendRedirect("index.jsp");
	}
	
	/**
	 * 注册
	 * @param request 请求
	 * @param response 响应
	 * @throws IOException
	 */
	private void regist(HttpServletRequest request, HttpServletResponse response) throws IOException {
		//获取用户名
		String name = request.getParameter("name");
		//用户直接访问
		if(name == null || name.isEmpty()) {
			//直接重定向到注册页面
			response.sendRedirect("regist.jsp");
			return;
		}
		//用户服务
		UserServiceImpl service = new UserServiceImpl();
		//打印机
		PrintWriter out = response.getWriter();
		//用户名查重
		if(service.getUserByName(name) != null) {
			//退回到注册页面
			out.print("<script>alert('此用户名已注册');history.back();</script>");
			return;
		}
		String pwd = request.getParameter("pwd");
		String mail = request.getParameter("mail");
		//注册
		if(service.regist(new UserInfo(name,pwd,mail))) {
			//跳转到登录页面
			out.print("<script>alert('注册成功！');location.href='index.jsp';</script>");
		} else {
			//跳转到注册页面
			out.print("<script>alert('注册失败！');history.back();</script>");
		}
	}
	
	/**
	 * 登录
	 * @param request 请求
	 * @param response 响应
	 * @throws IOException
	 */
	private void login(HttpServletRequest request, HttpServletResponse response) throws IOException {
		//登录
		//获取用户名
		String name = request.getParameter("name");
		//用户直接访问，没有传入用户名
		if(name == null || name.isEmpty()) {
			//直接重定向
			response.sendRedirect("index.jsp");
			return;
		}
		name = new String(name.getBytes("iso-8859-1"),"utf-8");
		//密码
		String pwd = new String(request.getParameter("pwd").getBytes("iso-8859-1"),"utf-8");
		UserInfo user = new UserInfo();
		user.setUsername(name);
		user.setPassword(pwd);
		PrintWriter out = response.getWriter();
		//登录
		if(new UserServiceImpl().login(user)) {
			//保存登录信息
			request.getSession().setAttribute("name", name);
			//跳转到登陆成功页面
			out.print("<script>alert('登录成功 ！');location.href='main.jsp';</script>");
			return;
		} else {
			//跳转到登录页面
			out.print("<script>alert('登录失败！');location.href='index.jsp';</script>");
			return;
		}
	}
}
