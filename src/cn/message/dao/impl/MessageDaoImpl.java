package cn.message.dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import cn.message.dao.JDBCUtils;
import cn.message.dao.MessageDao;
import cn.message.entity.Message;

public class MessageDaoImpl implements MessageDao {

	@Override
	public List<Message> getMessagesBySendto(String sendto) {
		String sql = "select * from msg where sendto = ?";
		try {
			return JDBCUtils.getQueryRunner().query(sql, new BeanListHandler<Message>(Message.class),sendto);
		} catch (SQLException e) {
			throw (RuntimeException) new RuntimeException("获取短信息异常").initCause(e);
		}
	}

	@Override
	public Message getMessageByMsgid(int msgid) {
		String sql = "select * from msg where msgid = ?";
		try {
			return JDBCUtils.getQueryRunner().query(sql, new BeanHandler<Message>(Message.class), msgid);
		} catch (SQLException e) {
			throw (RuntimeException) new RuntimeException("获取短信息失败").initCause(e);
		}
	}

	@Override
	public int update(Message msg) {
		String sql = "UPDATE msg SET title = ?,msgcontent = ?,state = ? WHERE msgid = ?";
		try {
			return JDBCUtils.getQueryRunner().update(sql,msg.getTitle(),msg.getMsgcontent(),
					msg.getState(),msg.getMsgid());
		} catch (SQLException e) {
			throw (RuntimeException) new RuntimeException("更新短信息异常").initCause(e);
		}
	}

	@Override
	public int delete(int msgid) {
		String sql = "delete from msg where msgid = ?";
		try {
			return JDBCUtils.getQueryRunner().update(sql,msgid);
		} catch (SQLException e) {
			throw (RuntimeException)new RuntimeException("删除短信息异常").initCause(e);
		}
	}

	@Override
	public int add(Message msg) {
		String sql = "INSERT INTO msg(username,title,msgcontent,state,sendto) VALUES(?,?,?,0,?)";
		try {
			return JDBCUtils.getQueryRunner().update(sql,msg.getUsername(),msg.getTitle(),msg.getMsgcontent(),
					msg.getSendto());
		} catch (SQLException e) {
			throw (RuntimeException)new RuntimeException("添加短信息异常").initCause(e);
		}
	}

}
