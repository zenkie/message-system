package cn.message.dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import cn.message.dao.JDBCUtils;
import cn.message.dao.UserDao;
import cn.message.entity.UserInfo;

public class UserDaoImpl implements UserDao {

	@Override
	public int login(UserInfo user) {
		String sql = "SELECT COUNT(*) FROM msg_userinfo WHERE username = ? AND `password` = ?";
		try {
			return JDBCUtils.getQueryRunner().query(sql, new ScalarHandler<Number>(),
					user.getUsername(),user.getPassword()).intValue();
		} catch (SQLException e) {
			throw (RuntimeException) new RuntimeException("登录异常").initCause(e); 
		}
	}

	@Override
	public int regist(UserInfo user) {
		String sql = "INSERT INTO msg_userinfo VALUES(?,?,?);";
		try {
			return JDBCUtils.getQueryRunner().update(sql,user.getUsername(),user.getPassword(),
					user.getEmail());
		} catch (SQLException e) {
			throw (RuntimeException) new RuntimeException("注册异常").initCause(e);
		}
	}

	@Override
	public UserInfo getUserByName(String name) {
		String sql = "select * from msg_userinfo where username = ?";
		try {
			return JDBCUtils.getQueryRunner().query(sql, new BeanHandler<UserInfo>(UserInfo.class),
					name);
		} catch (SQLException e) {
			throw (RuntimeException) new RuntimeException("获取用户异常").initCause(e);
		}
	}

	@Override
	public List<UserInfo> getOthers(String user) {
		String sql = "select * from msg_userinfo where username != ?";
		try {
			return JDBCUtils.getQueryRunner().query(sql, new BeanListHandler<UserInfo>(UserInfo.class),
					user);
		} catch (SQLException e) {
			throw (RuntimeException) new RuntimeException("获取其他用户异常").initCause(e);
		}
	}

}
