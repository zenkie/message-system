package cn.message.dao;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;

import com.mchange.v2.c3p0.ComboPooledDataSource;
/**
 * JDBC工具类
 * @author ZENKIE
 *
 */
public class JDBCUtils {
	//连接池
	private static ComboPooledDataSource ds = new ComboPooledDataSource();
	private static QueryRunner qr = new QueryRunner(ds);
	//私有化
	private JDBCUtils(){}
	/**
	 * 获取连接
	 * @return 连接
	 */
	public static Connection getConnection() {
		try {
			return ds.getConnection();
		} catch (SQLException e) {
			throw (RuntimeException) new RuntimeException("连接异常").initCause(e);
		}
	}
	
	/**
	 * 获取连接池
	 * @return 连接池
	 */
	public static DataSource getDataSource() {
		return ds;
	}
	public static QueryRunner getQueryRunner() {
		return qr;
	}
	
}
