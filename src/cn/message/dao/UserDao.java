package cn.message.dao;
/**
 * 用户数据访问类
 * @author ZENKIE
 *
 */

import java.util.List;

import cn.message.entity.UserInfo;

public interface UserDao {
	/**
	 * 登录
	 * @param user 用户
	 * @return 找到的用户数
	 */
	int login(UserInfo user);
	/**
	 * 注册
	 * @param user 用户
	 * @return 受影响的行数
	 */
	int regist(UserInfo user);
	/**
	 * 通过用户名获取用户
	 * @param name 用户名
	 * @return 用户
	 */
	UserInfo getUserByName(String name);
	/**
	 * 获取其他用户
	 * @return
	 */
	List<UserInfo> getOthers(String user);
}
