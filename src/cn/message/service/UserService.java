package cn.message.service;

import java.util.List;

import cn.message.entity.UserInfo;

public interface UserService {
	/**
	 * 登录
	 * @param user 用户
	 * @return 是否成功
	 */
	boolean login(UserInfo user);
	/**
	 * 注册
	 * @param user 用户
	 * @return 是否成功注册
	 */
	boolean regist(UserInfo user);
	/**
	 * 通过用户名获取用户
	 * @param name 用户名
	 * @return 用户
	 */
	UserInfo getUserByName(String name);
	/**
	 * 获取其他用户
	 * @param name 当前用户名
	 * @return 其他用户
	 */
	List<UserInfo> getOthers(String name);
}
