package cn.message.service.impl;

import java.util.List;

import cn.message.dao.MessageDao;
import cn.message.dao.impl.MessageDaoImpl;
import cn.message.entity.Message;
import cn.message.service.MessageService;

public class MessageServiceImpl implements MessageService {
	private static MessageDao dao = new MessageDaoImpl(); 
	@Override
	public List<Message> getMessagesBySendto(String sendto) {
		return dao.getMessagesBySendto(sendto);
	}
	
	@Override
	public Message getMessageByMsgid(int msgid) {
		return dao.getMessageByMsgid(msgid);
	}

	@Override
	public int update(Message msg) {
		return dao.update(msg);
	}

	@Override
	public int delete(int msgid) {
		return dao.delete(msgid);
	}

	@Override
	public int add(Message msg) {
		return dao.add(msg);
	}
}
