package cn.message.service.impl;

import java.util.List;

import cn.message.dao.UserDao;
import cn.message.dao.impl.UserDaoImpl;
import cn.message.entity.UserInfo;
import cn.message.service.UserService;

public class UserServiceImpl implements UserService {
	private UserDao dao = new UserDaoImpl();
	@Override
	public boolean login(UserInfo user) {
		return dao.login(user) > 0;
	}
	@Override
	public boolean regist(UserInfo user) {
		return dao.regist(user) > 0;
	}
	@Override
	public UserInfo getUserByName(String name) {
		return dao.getUserByName(name);
	}
	@Override
	public List<UserInfo> getOthers(String name) {
		return dao.getOthers(name);
	}

}
