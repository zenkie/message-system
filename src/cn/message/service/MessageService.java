package cn.message.service;

import java.util.List;

import cn.message.entity.Message;

public interface MessageService {
	/**
	 * 通过获取个人信息
	 * @param sendto 收件人
	 * @return 所有信息
	 */
	List<Message> getMessagesBySendto(String sendto);
	/**
	 * 获取一条短信息
	 * @param msgid 信息编号
	 * @return 短信息
	 */
	Message getMessageByMsgid(int msgid);
	/**
	 * 更新短信息
	 * @param msg 短信息
	 * @return 受影响的行数
	 */
	int update(Message msg);
	/**
	 * 删除短信息
	 * @param msgid 信息编号
	 * @return 受影响的行数
	 */
	int delete(int msgid);
	/**
	 * 添加短信息
	 * @param msg 要添加的短信息，需要设置标题、内容、发送者、收件人
	 * @return 受影响的行数
	 */
	int add(Message msg);
}
