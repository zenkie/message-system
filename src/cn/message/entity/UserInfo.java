package cn.message.entity;

import java.io.Serializable;

/**
 * 用户实体类，保存用户信息
 * @author ZENKIE
 *
 */
public class UserInfo implements Serializable {
	private String username;
	private String password;
	private String email;
	/**
	 * 创建用户
	 */
	public UserInfo() {
	}
	/**
	 * 创建用户
	 * @param username 用户名
	 * @param password 密码
	 * @param email 邮箱
	 */
	public UserInfo(String username, String password, String email) {
		this.username = username;
		this.password = password;
		this.email = email;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@Override
	public String toString() {
		return "UserInfo [username=" + username + ", password=" + password + ", email=" + email + "]";
	}
}
