package cn.message.entity;

import java.io.Serializable;
import java.util.Date;

public class Message implements Serializable {
	private int msgid;//短信编号
	private String username;//发件人
	private String title;//标题
	private String msgcontent;//内容
	private int state;//信息状态，0表示未读，1表示已读
	private String sendto;//收件人
	private Date msg_create_date;//发件时间
	public Message() {
	}
	public Message(int msgid, String username, String title, String msgcontent, int state, String sendto,
			Date msg_create_date) {
		this.msgid = msgid;
		this.username = username;
		this.title = title;
		this.msgcontent = msgcontent;
		this.state = state;
		this.sendto = sendto;
		this.msg_create_date = msg_create_date;
	}
	public int getMsgid() {
		return msgid;
	}
	public void setMsgid(int msgid) {
		this.msgid = msgid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getMsgcontent() {
		return msgcontent;
	}
	public void setMsgcontent(String msgcontent) {
		this.msgcontent = msgcontent;
	}
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}
	public String getSendto() {
		return sendto;
	}
	public void setSendto(String sendto) {
		this.sendto = sendto;
	}
	public Date getMsg_create_date() {
		return msg_create_date;
	}
	public void setMsg_create_date(Date msg_create_date) {
		this.msg_create_date = msg_create_date;
	}
	@Override
	public String toString() {
		return "Message [msgid=" + msgid + ", username=" + username + ", title=" + title + ", msgcontent=" + msgcontent
				+ ", state=" + state + ", sendto=" + sendto + ", msg_create_date=" + msg_create_date + "]";
	}
	
	
}
