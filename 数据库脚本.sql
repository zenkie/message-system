/*
SQLyog Ultimate v8.32 
MySQL - 5.7.25 : Database - messagesystem
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`messagesystem` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `messagesystem`;

/*Table structure for table `msg` */

DROP TABLE IF EXISTS `msg`;

CREATE TABLE `msg` (
  `msgid` int(11) NOT NULL AUTO_INCREMENT COMMENT '短信息id',
  `username` varchar(20) NOT NULL COMMENT '短信息发送方',
  `title` varchar(40) NOT NULL COMMENT '标题',
  `msgcontent` varchar(400) NOT NULL COMMENT '内容',
  `state` int(11) NOT NULL COMMENT '消息状态(已读/未读)',
  `sendto` varchar(20) NOT NULL COMMENT '短信息接收方',
  `msg_create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '消息发送时间',
  PRIMARY KEY (`msgid`),
  KEY `fk_msg_username` (`username`),
  KEY `fk_msg_sendto` (`sendto`),
  CONSTRAINT `fk_msg_sendto` FOREIGN KEY (`sendto`) REFERENCES `msg_userinfo` (`username`),
  CONSTRAINT `fk_msg_username` FOREIGN KEY (`username`) REFERENCES `msg_userinfo` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='短信息表';

/*Data for the table `msg` */

/*Table structure for table `msg_userinfo` */

DROP TABLE IF EXISTS `msg_userinfo`;

CREATE TABLE `msg_userinfo` (
  `username` varchar(20) NOT NULL COMMENT '用户名',
  `password` varchar(20) NOT NULL COMMENT '密码',
  `email` varchar(20) NOT NULL COMMENT '邮箱地址',
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户表';

/*Data for the table `msg_userinfo` */

insert  into `msg_userinfo`(`username`,`password`,`email`) values ('admin','admin','admin@admin.cn');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
