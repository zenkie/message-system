<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="text/css" rel="stylesheet" href="css/main.css"/>
<!-- 登录验证 -->
<c:if test="${empty name}">
	<jsp:forward page="index.jsp"/>
</c:if>
<title>我的短信息 - ${name}</title>
<script type="text/javascript" src="js/jquery-1.12.4.js"></script>
<script>
	$(function(){
		$(".imgLink").click(
			function(){
				$(this).find("img")[0].setAttribute("src","img/sms_readed.png");
			}
		);
	});
	function del(msgid) {
		if(confirm("确定要删除这条消息吗？")) {
			location.href="message?action=delete&msgid=" + msgid;
		}
	}
</script>
</head>
<body>
	<header>
		<img src="img/sms_my_message.png" id="headpic"/>
		<div class="status">
			<div class="right">
			当前用户：<a href="message?action=main"><strong>${name}</strong></a>
			<a href="user?action=send"><strong>发短消息</strong></a>
			<a href="user?action=logout"><strong>退出</strong></a>
			</div>
		</div>
	</header>
	<!-- 如果没有内容，需要获取 -->
	<c:if test="${requestScope.msgs == null }">
		<jsp:forward page="message?action=main"/>
	</c:if>
	<article>
		<c:forEach var="msg" items="${requestScope.msgs }">
			<div class="msg">
				<!-- 已读状态 -->
				<a href="message?action=read&msgid=${msg.msgid}" class="imgLink">
					<c:choose>
						<c:when test="${msg.state == 0}">
							<img src="img/sms_unReaded.png"/>
						</c:when>
						<c:otherwise>
							<img src="img/sms_readed.png"/>
						</c:otherwise>
					</c:choose>
				</a>
				<span class="mention">
					<a href="message?action=read&msgid=${msg.msgid}">
						<!-- 标题 -->
						<strong>${msg.title}</strong>
						<!-- 内容预览 -->
						<c:choose>
							<c:when test="${msg.msgcontent.length() < 10}">
								${msg.msgcontent}
							</c:when>
							<c:otherwise>
								${msg.msgcontent.substring(0,7)}...
							</c:otherwise>
						</c:choose>
					</a>
				</span>
				<div class="right">
					<a href="javascript:del(${msg.msgid})">删除</a>
					<a href="user?action=send&msgid=${msg.msgid}">回复</a>
					<fmt:formatDate value="${msg.msg_create_date}" pattern="yyyy-MM-dd"/>
				</div>
			</div>
		</c:forEach>
	</article>
</body>
</html>