<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="text/css" rel="stylesheet" href="css/send.css"/>
<%-- 登录验证 --%>
<c:if test="${empty name}">
	<jsp:forward page="index.jsp"/>
</c:if>
<title>我的短信息 - ${name}</title>
<script type="text/javascript" src="js/jquery-1.12.4.js"></script>
<script>
	$(function(){
		$(".imgLink").click(
			function(){
				$(this).find("img")[0].setAttribute("src","img/sms_readed.png");
			}
		);
	});
</script>
</head>
<body>
	<header>
		<img src="img/sms_my_message.png" id="headpic"/>
		<div class="status">
			<div class="right">
			当前用户：<a href="main.jsp"><strong>${name}</strong></a>
			<a href="user?action=send"><strong>发短消息</strong></a>
			<a href="user?action=logout"><strong>退出</strong></a>
			</div>
		</div>
	</header>
	<%-- 如果没有其他用户，需要获取 --%>
	<c:if test="${empty others}">
		<jsp:forward page="main.jsp"/>
	</c:if>
	<form action="message?action=send" method="post">
		<article>
			<div class="title">
				发送给：
				<select name="sendto">
					<c:forEach var="user" items="${others}">
						<option value="${user.username }" <c:if test="${user.username == msg.username}">selected</c:if>>
						${user.username }
						</option>
					</c:forEach>
				</select>
				标题：
				<c:choose>
					<c:when test="${empty msg}">
						<input type="text" name="title" value=""/>
					</c:when>
					<c:otherwise>
						<input type="text" name="title" value="【回复】:${msg.title}"/>
					</c:otherwise>
				</c:choose>
			</div>
			<div class="content">
				<textarea name="content" rows="20" cols="120"></textarea>
				<br/><br/>
				<input type="image" src="img/sms_send.gif"/>
			</div>
		</article>
	</form>
</body>
</html> 