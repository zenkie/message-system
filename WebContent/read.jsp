<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="text/css" rel="stylesheet" href="css/read.css"/>
</head>
<!-- 登录验证 -->
<c:if test="${empty name}">
	<jsp:forward page="index.jsp"/>
</c:if>
<!-- 如果没有内容，需要获取 -->
<c:if test="${requestScope.msg == null }">
	<jsp:forward page="message?action=read"></jsp:forward>
</c:if>
<title>阅读消息 - ${msg.title}</title>
<body>
	<header>
		<img src="img/sms_read_message.png" id="headpic"/>
		<div class="status">
			<div class="right">
			当前用户：<a href="main.jsp"><strong>${name}</strong></a>
			<a href="user?action=send"><strong>发短消息</strong></a>
			<a href="user?action=logout"><strong>退出</strong></a>
			</div>
		</div>
	</header>
	<article>
		<div class="title">
			题目：${msg.title } 来自：${msg.username }
			时间：<fmt:formatDate value="${msg.msg_create_date}" pattern="yyyy-MM-dd"/>
		</div>
		<div class="content">
			${msg.msgcontent}
		</div>
	</article>
</body>
</html>