<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link type="text/css" rel="stylesheet" href="css/login.css"/>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>登录</title>
</head>
<body>
	<header>
		<img src="img/sms_login_title.png"/>
	</header>
	<article>
		<form action="user?action=login" method="post">
			<table>
				<tr>
					<td class="title">用户名</td>
					<td>
						：<input type="text" name="name" required/>
					</td>
				</tr>
				<tr>
					<td class="title">密码</td>
					<td>
						：<input type="password" name="pwd" required/>
					</td>
				</tr>
				<tr>
					<td align="center" colspan="2">
						<input type="image" src="img/sms_btn_login.png"/>
						<a href="regist.jsp"><img src="img/sms_btn_reg.png"/></a>
					</td>
				</tr>
			</table>
		</form>
	</article>
</body>
</html>