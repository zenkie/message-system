<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link type="text/css" rel="stylesheet" href="css/regist.css"/>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<script>
	function check() {
		if(document.getElementById("pwd").value != document.getElementById("repwd").value)) {
			alert("两次输入的密码不一致！");
			return false;
		} else {
			return true;
		}
	}
</script>
<body>
	<header>
		<img src="img/sms_reg_title.png"/>
	</header>
	<article>
		<form action="user?action=regist" method="post">
			<table>
				<tr>
					<td class="title">用户名</td>
					<td>
						：<input type="text" id="name" name="name" required pattern="\w{1,18}"/>
					</td>
				</tr>
				<tr>
					<td class="title">密码</td>
					<td>
						：<input type="password" id="pwd" name="pwd" required pattern="\w{1,20}"/>
					</td>
				</tr>
				<tr>
					<td class="title">确认密码</td>
					<td>
						：<input type="password" id="repwd" required pattern="\w{1,20}"/>
					</td>
				</tr>
				<tr>
					<td class="title">邮箱</td>
					<td>
						：<input type="email" name="mail"/>
					</td>
				</tr>
				<tr>
					<td align="center" colspan="2">
						<input type="image" src="img/sms_btn_reg.png" onclick="check()" />
						<input type="image" src="img/sms_btn_reset.png" onclick="reset();return false;"/>
						<br/><a href="login.jsp"><img alt="回到主页" src="img/sms_btn_goback.png"></a>
					</td>
				</tr>
			</table>
		</form>
	</article>
</body>
</html>