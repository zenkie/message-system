# 消息系统

#### 介绍
一个简单的学习案例，可以在不同用户间发送消息

#### 软件架构
语言：Java、JSP/Servlet、JDBC

#### 使用说明

需要将项目部署到Tomcat中，可参考以下教程：https://jingyan.baidu.com/article/22a299b5c6cfb09e18376a62.html

